package com.example.demo.repository;

import com.example.demo.model.dto.EmployeesDTO;
import com.example.demo.model.entity.EmployeesEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<EmployeesEntity, Integer> {

    List<EmployeesDTO> findAllByFirstNameOrLastName(String firstParameter, String secondParameter);
}
