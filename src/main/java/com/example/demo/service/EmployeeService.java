package com.example.demo.service;

import com.example.demo.model.dto.EmployeesDTO;
import com.example.demo.model.entity.EmployeesEntity;

import java.util.List;

public interface EmployeeService {

    List<EmployeesDTO> findByText(String text);

    List<EmployeesEntity> findAll();
}
