package com.example.demo.service.impl;

import com.example.demo.model.dto.EmployeesDTO;
import com.example.demo.model.entity.EmployeesEntity;
import com.example.demo.repository.EmployeeRepository;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository repository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.repository = employeeRepository;
    }

    @Override
    public List<EmployeesDTO> findByText(String text) {
        return repository.findAllByFirstNameOrLastName(text, text);
    }

    @Override
    public List<EmployeesEntity> findAll() {
        List<EmployeesEntity> results = new ArrayList<>();
        repository.findAll().forEach(results::add);
        return results;
    }
}
