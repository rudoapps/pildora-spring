package com.example.demo.controller;

import com.example.demo.model.entity.EmployeesEntity;
import com.example.demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/employee", produces = MediaType.APPLICATION_JSON_VALUE)
public class EmployeeController {

    private final EmployeeService service;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.service = employeeService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity getAll(@RequestParam(value = "length", defaultValue = "100") Integer size) {
        List<EmployeesEntity> results = service.findAll();
        return ResponseEntity.ok(results.size() > size ? results.subList(0, size) : results);
    }

    @GetMapping("/byName")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity searchByNombre(@RequestParam("text") String texto) {
        return ResponseEntity.ok(service.findByText(texto));
    }
}
