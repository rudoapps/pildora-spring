#Repositorio de aplicación Java2EE con Spring

Este repositorio pretende servir como ejemplo de creación de manera rápida de una aplicación J2EE para desplegar en servidores de aplicaciones Java usando el **Spring Framework**.

El servicio que ofrecerá serán servicios REST.

---

## Requisitos del proyecto

Para preparar el entorno necesario de ejecución, necesitamos tener instaladas las siguientes dependencias en tu sistema local:

1. JDK.
2. Maven.
2. IntelliJ IDEA.
3. XAMPP (podría usarse otro).
4. Wildfly (opcional).

---

## Preparación del proyecto

Teniendo todas las dependencias instaladas, procedemos a la instalación y configuración del entorno de desarrollo.

1. Iniciamos nuestro software XAMPP.
2. El único requisito inicial es iniciar la base de datos MySQL.
3. Se da por supuesto conexión y configuración funcional con ella.
4. Descargamos la base de datos de ejemplo a nuestra carpeta de descargas. ([GitHub](https://github.com/datacharmer/test_db), [Tutorial](https://dev.mysql.com/doc/employee/en/employees-installation.html)).

```
Hace unas versiones que XAMPP provee su instalador con MariaBD en lugar de MySQL.
De todos modos esto no tiene especial importancia para este tutorial.
```

---

## Creación del proyecto

El siguiente paso será la creación del proyecto J2EE a través del asistente de configuración de Intellij, que tiene una función de creación rápida de un proyecto que integre el **Framework Spring** usando su asistente **Spring Initializr**.

* Iniciamos IntelliJ IDEA, de ahora en adelante referido solo con IntelliJ, y en su diálogo inicial, seleccionamos la opción **Create New Project**.
* Seleccionaremos la opción **Spring Initializr** del menú lateral, en la cual dejaremos sus opciones marcadas como vienen por defecto y apretamos sobre el botón de **Next**.
* Tras obtener las opciones y configuraciones posibles que nos permite la herramienta *Spring Initializr*, nos mostrará el diálogo para introducir los datos que se usarán para nombrar e identificar al proyecto. Lo configuraremos del siguiente modo:
    * Datos referentes a tu proyecto.
    * Packaging: War
* El siguiente paso será especificarle a la herramienta las dependencias quieres que se integren en el proyecto. Cuantas más opciones elijas, de más funcionalidad se puede proveer a nuestra aplicación.
Por lo que las herramientas que vamos a seleccionar para proveer de la funcionalidad mínima deseada a nuestra aplicación son las siguientes:
    * Core
        * DevTools
    * Web
        * Web
    * SQL
        * JPA
        * MySQL
* Por último especificamos el nombre del proyecto y su ubicación en disco.
* Tras el tiempo necesario para poder generar y bajar el proyecto generado de **Spring Initializr**, se abrirá IntelliJ con el proyecto generado mostrando un pequeño aviso en la esquina inferior derecha. Seleccionaremos **Enable Auto-Import**. De este modo se configurará Maven para que autogenere la configuración del proyecto, y baje las dependencias necesarias conforme las vaya necesitando.

![Interfaz principal IntelliJ](https://bitbucket.org/mobilendo/pildora-spring/raw/master/images/001.png)

```
Especial cuidado con la versión de Spring seleccionada en el paso 4. En cada versión cambian la manera de hacer las cosas.**
Por compatibilidad con este tutorial, se usará la versión 1.5.13 de Spring Framework.

Las dependencias seleccionadas se pueden ver en el lateral derecho del paso 5 etiquetado como **Selected Dependencies**.
```

## Importación en Base de Datos.

### Configuración de la conexión en el IDE

Ahora es el momento de aprovechar los datos que hemos descargado previamente. Importaremos una base de datos, con datos de ejemplo con el fin de ahorrar tiempo en su definición.

Vamos a configurar IntelliJ para llevar la gestión de la BD desde el IDE.

Para ello usaremos la vista **Database** del menú lateral del IDE.
* View > Tool Buttons
* View > Tool Windows > Database

Ahora seleccionamos la opción para importar un nuevo origen de datos con MySQL.
* (+) > Data Source > MySQL

En el diálogo de configuración rellenamos todos los datos pertinentes a la conexión que establecerá con nuestro MySQL.
**Recuerda hacer click en _Download missing driver files_** para que la librería de MySQL se agregue al IDE y permita las conexiones de manera correcta.

Por último seleccionamos **Test connection**. El resultado debe escribir en verde la palabra _Successful_.
Por último apretaremos sobre **OK**.

###Importación del contenido descargado

La importación debe hacerse de manera nativa desde la consola, para ello accedemos a la interfaz de XAMPP y apretamos en la opción `Open Terminal` dentro de la pestaña `General`.

Esto nos abrirá una consola con una sesión ssh abierta. Nos descargamos la base de datos de ejemplo a través de esta terminal:

```bash
root@debian:~# apt-get install unzip
root@debian:~# wget https://github.com/datacharmer/test_db/archive/master.zip
root@debian:~# unzip master.zip
root@debian:~# cd test_db-master/
## Asegurate primero que las tres primera líneas (DROP, CREATE, USE) contienen el nombre del esquema que has creado.
root@debian:~# mysql -t -u root < employees.sql
```

La importación de datos llevará un tiempo.

Para comprobar que la importación ha resultado de manera exitosa, refrescamos y desplegamos del panel **Database** el elemento con nuestra BD, desplegamos **schemas** y dentro de este seleccionamos el esquema que contendrá todo nuestro contenido.

Dentro de este veremos la estructura de la tabla incluyendo todo su contenido.

De este modo, tendremos una base de datos de ejemplo con casi 4M de entradas, perfecta para representar valores a través de un servicio REST.

## Proyecto Java2EE con Spring

**Spring Initializr** por defecto genera un proyecto mono-módulo. Lo que sería más interesante sería hacerlo multi-módulo, para generar una abstracción entre capas y con ello abstraer funcionalidad para poder compartirla entre módulos.
Para el ejemplo que vamos a tratar ahora, vamos a dejarlo mono-módulo.

La estructura que se va a usar, va a ser una estructura por capas, separando, aislando y abstrayendo entre capas las funcionalidades. Haciendo que sigamos un Modelo Vista Controlador (MVC).

Por lo que vamos a generar una estructura de proyecto, emulando esa separación por capas del siguiente modo (partiendo de src.main.java.com.example.demo):

```
.
├── controller
│   ├── EmployeeController.java
│   └── ...
├── service
│   ├── impl
│   │   ├── EmployeeServiceImpl.java
│   │   └── ...
│   ├── EmployeeService.java
│   └── ...
├── repository
│   ├── EmployeeRepository.java
│   └── ...
└── model
    ├── entity
    │   ├── EmployeeEntity.java
    │   └── ...
    └── dto
        ├── EmployeeDTO.java
        └── ...
```

### Contenidos de los directorios

* **controller**:

    Bajo este directorio, tendremos los controladores REST, aquellos que pertenecen a la capa de presentación, encargados de exponer el endpoint y definir los verbos HTTP que cada uno de ellos soportan. Proporcionan un punto de entrada de tu aplicación.

* **service**:

    Contiene las interfaces que proveen del funcionamiento de los servicios, y sus implementaciones. Estos hacen referencia a la capa de negocio, donde se albergará toda la lógica de tu aplicación.

* **repository**:

    Los objetos del repositorio, son los encargados de almacenar la lógica de obtención de datos a más bajo nivel. Este contendrá las interfaces de **Spring Data** que son implementadas de manera automática por **Spring** y construyen dinámicamente las consultas _SQL_.
    
    Son el equivalente a los **Objetos de acceso a datos (DAO)** en el patrón de diseño. 

* **model**:

    El directorio de los modelos, contendrá dos elementos principalmente:
    
    * Entidades
    * Objetos de transferencia de datos (DTOs).
   
Ahora es el turno de ir rellenando las capas con sus respectivas funcionalidades.

#### Generación de un Model

##### Generación de un Entity


Vamos a generar todas las entidades en un solo paso, dado que IntelliJ tiene la funcionalidad de importar todos los elementos de un **Data Source** configurado.
Para ello hay que añadir un módulo de soporte al proyecto, que más tarde eliminaremos, ya que este genera las entidades con mucha rapidez, y tan solo introduce un fichero que también eliminaremos.

En la Vista de **1. Project** de IntelliJ, situada habitualmente en el lateral izquierdo de la pantalla, aparece el árbol con la vista del proyecto.

Hacemos clic derecho sobre el módulo raíz, y seleccionamos **Add Framework Support**. Buscamos y seleccionamos la opción **JavaEE Persistence**. Marcamos el checkbox y nos permitirá configurar la parte derecha del diálogo.

Por defecto, la implementación del _ORM_ que Spring configura por defecto, es **Hibernate**. Podemos seleccionar la opción que queramos, puesto que el fichero que va a generar esta opción, vamos a prescindir de él. Lo que si que es importante es marcar el checkbox de **Import Database Schema**.
La librería a usar, es la que se importa a través de _Maven_, tal cual viene especificado por defecto.
Esto mostrará al acabar este proceso el diálogo .

Tras apretar en _OK_, nos muestra un diálogo de generación de Entidades, a partir de la conexión definida anteriormente, que nos permite seleccionar que elementos deseamos importar desde la BD, tal y como podemos apreciar en la siguiente captura:

Vamos a seleccionar solo las tablas, ya que la generación de vistas no es tan cómoda. La modificación del tipo del parámetro _gender_ de la tabla _employee_ ha sido necesaria para poder generarlo. Por defecto usa `java.lang.Object` y hay que usar `java.lang.String`.

También es conveniente revisar los tipos de las fechas. Por defecto se usa `java.sql.Date` y Spring tiene dificultades a la hora de autotransformarlas, es conveniente usar `java.util.Date`.
 
![Diálogo creación entidades](https://bitbucket.org/mobilendo/pildora-spring/raw/master/images/002.png)

Tras la importación, hay un par de repasos que es necesario realizar para que las entidades funcionen de manera correcta:

* Eliminación del parámetro `catalog` y su valor, del interior de la anotación `@Table` de todas las clases anotadas como `@Entity`.
* Eliminación del soporte JPA (que gestionará Spring, en lugar del recientemente importado).
    * Botón derecho en la raiz del proyecto > Open Module Settings > Modules > pildora-spring > JPA > (-)
* Eliminación del fichero `src/main/java/META-INF/persistence.xml`.

##### Generación de un DTO

El proceso de creación del DTO es manual, y por tanto hay que generar una estructura de datos que represente de manera fiel el contenido de la entidad, suprimiendo toda aquella información, que se considere **no** necesaria.

```
Cuidado con las referencias de unas clases a otras. 
Es posible generar DTO's que referencien a otros DTO's
y que en la serialización para su transmisión sean enviados juntos. 
Es un proceso habitual que estos generen una redundancia cíclica, 
y por tanto la serialización entre en bucle. 
Es algo muy habitual cuando serializas directamente una entidad 
con claves ajenas o entidades con claves primarias siendo referenciadas 
como claves ajenas en otras tablas.
```

Por lo tanto generamos un DTO de ejemplo para representar el contenido de la entidad `Employee.java`.

Basta con copiar los atributos que contenga la entidad, suprimir aquello que no deseemos, y generar constructor con los getters y setters de este a su vez que el toString por comodidad.

```java
public class EmployeesDTO {
    private int empNo;
    private Date birthDate;
    private String firstName;
    private String lastName;
    private String gender;
    private Date hireDate;
    
    /*
     * Constructor 
     */

    /*
     * Getters y Setters
     */
    
    /*
     * toString 
     */
}
```

```
Es interesante nombrar que los nombres de los atributos deben ser los mismos. De este modo no hay que realizar acciones adicionales a posterior.
También hay que fijarse en el import de la clase Date. También debe ser la clase Date del paquete util de Java.
```

También sería muy cómodo generar un método que sea capaz de recibir una *Entity*, y generar un DTO a partir de esta. Pero eso ya es opcional, puesto que Spring trata de convertirlas usando *Spring Data*.

#### Generación de un Repository

Es el momento de implementar la solución que usa *Spring* para la representación de los DAO.

Spring usa por defecto unas palabras clave en los nombres de los métodos para que internamente se encargue de implementar las consultas sin necesidad de la intervención del desarrollador.

El conjunto de palabras clave que se usan se pueden consultar en su documentación oficial [aquí](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation)

Es tambien una práctica habitual, generar una interfaz genérica parametrizada, a la que se le configuren una serie de funcionalidades comunes entre repositorios para aprovechar después por herencia y de este modo evitar duplicidad de código. Para nuestro caso, Spring provee la interfaz CrudRepository, que provee este esquema básico común que necesitamos por ahora. 

CrudRepository provee todas las acciones CRUD básicas para un repositorio:

* **C**reate
* **R**ead
* **U**pdate
* **D**elete

Ahora vamos nosotros a generar el primer Repository para la entidad `Employee.java`. Vamos a nombrarlo `EmployeeRepository.java`.
La funcionalidad que vamos a implementar es buscar todos los empleados que cuadre su nombre o su apellido con la cadena recibida. 

```java
public interface EmployeeRepository extends CrudRepository<EmployeesEntity, Integer> {

    List<EmployeesDTO> findAllByFirstNameOrLastName(String firstParameter, String secondParameter);
}
```

Por ahora esta funcionalidad es suficiente. Contemos con los métodos por defecto, ya implementados por Spring:
* save
* findOne
* exists
* findAll
* count
* delete
* deleteAll

```
IntelliJ es bastante inteligente, y te asiste con la creación de los nombres usando las Keywords de Spring, por lo que esto facilita mucho el trabajo.
```

#### Generación de un Service

En este punto, es el mejor momento para comentar el método usado mayoritariamente en Spring para la inyección de dependencias. Hay varias maneras, pero vamos a comentar la que bajo mi punto de vista es la más apropiada.

Dado que la gestión de dependencias se realiza en el momento de la invocación a esta dependencia, la manera más optima de usarla, es la inyección de dependencias a través del contructor. Es posible anotar los parámetros también individualmente, pero el objetivo es invocar el contructor del objeto una sola vez, lo cual haría que las dependencias se instancien una sola vez, por lo que las referencias a los objetos se compartirían entre Beans, a fin de optimizar la memoria, y por tanto compartir las dependencias.

La manera de hacer esto, es haciendo uso de la anotación `@Autowired` en el contructor de la clase. Así se le indica a Spring que para construir el objeto, es necesaria la instanciación previa de sus dependencias. Se podría decir que se guardan las referencias a todos los objetos, para inyectar esa referencia a cada clase que la necesite, por lo que solo la instanciaría en caso de ser necesaria, sino pasaría la referencia al mismo.

Primero vamos a generar la interfaz que definirá el comportamiento de los servicios. De este modo cumpliremos el modelo de implementación usado en Spring para servicios, haciendo uso de las interfaces y las implementaciones a estas. 

Tras esto, vamos a generar la implementación del servicio, de modo que tenga dependencia del repositorio. Dado que el repositorio es una interfaz, y que este requiere la intervención de Spring para que implemente sus métodos, la instanciación de la clase `EmployeeService.java` debe esperar a que su repositorio (recibido en su contructor) sea implementado e instanciado por Spring. 
Es importante anotar la importancia de la anotación `@Service`, dado que esto indica a Spring, que la clase implementada cumple con el estereotipo de servicio, de este modo saber que contendrá lógica de negocio, y optimizar de cierto modo su funcionamiento. 

##### Interfaz:
```java
public interface EmployeeService {
    
    List<EmployeesDTO> findByText(String text);
    
    List<EmployeesEntity> findAll();
}
```

##### Clase:
```java
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository repository;

    @Autowired
    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.repository = employeeRepository;
    }

    @Override
    public List<EmployeesDTO> findByText(String text) {
        return repository.findAllByFirstNameOrLastName(text, text);
    }

    @Override
    public List<EmployeesEntity> findAll() {
        List<EmployeesEntity> results = new ArrayList<>();
        repository.findAll().forEach(results::add);
        return results;
    }
}
```

El motivo de transformar a una lista en el método `findAll`, es porque Spring por defecto devuelve un `Iterable<T>`, y es más cómodo trabajar con una colección. 

#### Generación de un Controller

Ahora es el turno del controlador REST. Este es el último punto de la capa de aplicación por el que debe pasar nuestra aplicación.
Esta capa será la encargada de entregar los resultados al cliente que lo solciite, y decidir de que modo devolverá la respuesta.

Los controladores permiten la obtención de cualquiera de los elementos que recibe desde la petición:
* Header params
* Path params
* Query params
* Body
* Otros

El método de asociación de estos parámetros a objetos, es mediante el uso de anotaciones. Existen distintos tipos de anotaciones para obtener cada uno de ellos:
* Header params: `@RequestHeader`
* Path params `@PathVariable`
* Query params `@RequestParam`
* Body `@RequestBody`

Cada una de estas anotaciones se pueden usar con todos los valores por defecto (`@RequestParam`) o estableciendo un valor a aquellos campos que se desee (`@RequestParam(value="nombreDeParametro", defaultValue="valorPorDefecto")`). También se puede modificar solo el campo value sin especificar el nombre del campo (`@RequestParam("myparam")`). 

La clase debe estar anotada como `@RestController` y también como `@RequestMapping`. Como antes, es posible establecer solo el campo value, pero todos los campos que establezcamos a nivel de clase, se heredarán en cada método. Por ejemplo, si configuramos que todos produzcan un resultado `application/json`, todos devolverán ese `Content-type` a no ser que ellos especifiquen lo contrario.
Este es también el encargado de definir cual es el *context path* que precederá a los definidos en cada método.

En cada método es posible especificar el código de respuesta en caso de no retornarlo en el método. Esto para métodos `void` es muy cómodo, ya que te evita devolver una Response solo para decir *200 OK*.

Si se desea manejar el código de respuesta que se genere según unos casos determinados, es posible decirle al método que retornará un objeto de tipo `ResponseEntity`. Este contiene métodos para generar cualquier tipo de respuesta.

La clase que nosotros generamos será como sigue:

```java
@RestController
@RequestMapping(value = "/employee", produces = MediaType.APPLICATION_JSON_VALUE)
public class EmployeeController {

    private final EmployeeService service;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.service = employeeService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity getAll(@RequestParam(value = "length", defaultValue = "100") Integer size) {
        List<EmployeesEntity> results = service.findAll();
        return ResponseEntity.ok(results.size() > size ? results.subList(0, size) : results);
    }

    @GetMapping("/byName")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity searchByNombre(@RequestParam("text") String texto) {
        return ResponseEntity.ok(service.findByText(texto));
    }
}
```

De este modo el controlador generará respuestas con los objetos que reciba.
Aquí entra en acción otra función de Spring, que es la de serialización del contenido de las respuestas.

Spring por defecto, usa Jackson, que es una librería de serialización de clases a JSON. Por lo que Jackson se encargará de recibir una clase y serializarla para poder transmitirla en manera representacional y no como objeto Java.
Llegados a este punto también se puede comentar la existencia de anotaciones de Jackson para poder modificar su comportamiento por defecto, como podría ser ignorar propiedades u otros.

### Configuración del fichero application.properties

El fichero `application.properties` es con diferencia uno de los más importantes de una aplicación Spring, dado que es el configurador de la aplicación. Una gran cantidad de parámetros son configurables desde aquí de manera centralizada.

Además, Spring funciona con lo que se conocen como *starters*. La función de los *starters* es la de añadir funcionalidad a Spring de manera centralizada y cómoda.

Por ejemplo, al principio de este tutorial hemos usado el **Initialzr** de *Spring*, que nos ha simplificado la tarea de incluir todas las dependencias y starters necesarios para poder usar todas las funcionalidades de las que hemos hecho uso.

Estos *starters* pueden consultarse desde el fichero `pom.xml` de nuestro proyecto Maven.

Las claves que hemos usado en esta aplicación son las siguientes, las cuales vienen explicadas, pero existen una infinidad de claves. Cada starter que importes puedes traer más.

```properties
# URL and credentials of MySQL database
spring.datasource.url = jdbc:mysql://localhost:3306/pildora?useSSL=false
spring.datasource.username = pildora
spring.datasource.password = 69UfYqQPcBkJBE2E

# Context path of REST application
server.contextPath = /api

# Show or not log for each sql query
spring.jpa.show-sql = true

# Well formatted queries
spring.jpa.properties.hibernate.format_sql = true

# Date format string or a fully-qualified date format class name
spring.jackson.date-format = yyyy-MM-dd HH:mm:ss

# Allows Hibernate to generate SQL optimized for a particular DBMS
spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect

# Makes Hibernate work better and safer
spring.jpa.hibernate.naming.strategy = org.hibernate.cfg.ImprovedNamingStrategy

# Hides query statistics to make log read easier
logging.level.org.hibernate.stat = false
```

Pueden consultarse todas las claves en la documentación oficial de Spring [aquí](https://docs.spring.io/spring-boot/docs/current/reference/html/common-application-properties.html).

Con esto ya podemos ejecutar nuestra aplicación Spring y comprobar que el resultado programado funciona correctamente tal y como esperamos.

Por defecto al ejecutar nuestra aplicación, se ejecuta en un Tomcat embebido, que despliega automáticamente la aplicación sobre nuestro localhost en el puerto 8080: [http://localhost:8080/api/employee](http://localhost:8080/api/employee).